gin
===

Google Interview

# Installation

    $ git clone https://bitbucket.org/__mek__/gin
    $ cd gin
    $ sudo pip install .

Installation will result in 3 command line utilities (listed below)
    
## Battleship

Battleship is a command line arcade game played over telnet. To run a
telnet server on port 1337:

    $ battleship -p 1337

By default, the server will run on port 23 (telnet) and sudo
permission escalation will likely be required.

## Flint

Flint is a python command line utility which uses naive dependency
injection to create stand-alone python source files. Assuming you have
a linter running on localhost, you can pipe the output of flint to
your lint service like follows:

    $ flint -f main.py | curl -d @- 127.0.0.1:8080/lint

## Combobreaker

Given a combination length and a number of values/choices for each
combination digit, combobreaker will exploit De Bruijn graphs to
produce a near-optimal input sequence for cracking the passcode.

    $ combobreaker -l4 -c10

## Minsq

Minsq is a program which solves the minimum list of square numbers
necessary which sum to a number `n` in O(sqrt(n) * n) time.

    $ minsq -n 12
    [2, 2, 2]
