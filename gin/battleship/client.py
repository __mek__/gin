
from twisted.protocols.basic import LineReceiver
from errors import GameFull

class Client(LineReceiver):

    def __init__(self, server):
        self.server = server
        self.game = None
        
    def disconnect(self):
        if getattr(self, 'player', None):
            del self.server.clients[self.id]

    def connectionMade(self):
        self.disconnect()
        self.send("Welcome, player: %s" % self.id)
        self.send("[b]rowse games, [j]oin a game, or [c]reate a new game?")
        
    def connectionLost(self, reason):
        self.disconnect()
        self.transport.loseConnection()

    def lineReceived(self, line):
        """Process client's commands"""
        if line:
            print(line)
            if line == ("b"):
                self.send("\n".join(str(game) for game in self.server.games))
            if line.startswith("j "):
                g = line.split("j ")[1]
                try:
                    self.game = self.server.games[g].add(self.server.clients[self.id])
                except GameFull:
                    self.send("Sorry, someone else joined first.")
            if line == ("c"):
                self.game = self.server.create_game(self.server.clients[self.id])
                self.send("Waiting for another player...")

    def send(self, msg, protocol=None):
        protocol = protocol if protocol else self
        protocol.sendLine("%s" % msg)

    def broadcast(self, msg, protocol=None, send2self=True):
        for uuid, protocol in self.server.clients.iteritems():
            if not send2self and uuid == self.server.clients[self.id]:
                continue
            self.send(msg, protocol=protocol)
