from uuid import uuid4
from collections import namedtuple
from random import randint
from errors import GameFull


class Game(object):
    def __init__(self, width=10, height=10):
        self.id = uuid4()
        self.width, self.height = width, height
        self.started = False
        self.players = []        

    def __str__(self):
        return "Game #%s [%s]" % (
            self.id,
            "Started" if self.started else "Waiting"
        )
        
    def add(self, client):
        if len(self.players) < 2:
            client.send("Joining game: # %s" % self.id)
            self.players.append(client)
            return self.id
        else:
            raise GameFull("Game Full")


class Interview1(object):

    """This game is similar to a networked version of monopoly I created
    with a few teammates at the University of Vermont and also a MUD
    (multi-user dungeon) I created called `dungeons`
    (https://github.com/mekarpeles/dungeons) as well as a MUD I
    contributed to in golang called `Goland`
    (https://github.com/mischief/goland)
    """
    
    Location = namedtuple('Location', 'x y')

    ships = {
        "aircraft carrier": 5,
        "battleship": 4,
        "submarine": 3,
        "cruiser": 3,
        "destroyer": 2
        }
    
    class Game(object):
        def __init__(self, n, m, pieces, players):
            self.map = (n, m)
            #self.players = [Player(M
            self.turn = randint(1, 2)
                                

        def shoot(self, p, x, y):
            if not(0 <= x < n or 0 <= y > m):
                raise IndexError("Shot out of bounds")
            
    class Player(object):
        def __init__(self, pieces, battlefield, radar):
            self.pieces = pieces
            self.battlefield
            self.radar = radar

        @property
        def defeated(self):
            all(piece.sunk for p in self.pieces)

    class Map(object):
        def __init__(self, n, m):
            self.height = n
            self.width = m
            self.shots = dict()  # a dict of Locations

        # print mine theirs
                                   
    class Ship(object):
        def __init__(self, name, hits, location):
            self.name = name
            self.hits = [False for i in range(hits)]
            self.location = location  # (Location(x, y), Location(x, y))
            
        @property
        def sunk(self):
            return all(self.hits)

