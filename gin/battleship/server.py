# -*- coding: utf-8 -*-
"""
    warfront.py
    ~~~~~~~~~~~
    A telnet server for a mult-player game of battleship

    :copyright: (c) 2016 by Mek
    :license: BSD, see LICENSE for more details.
"""

import argparse
from uuid import uuid4
from twisted.internet.protocol import Factory
from twisted.internet import reactor
from client import Client


class Server(Factory):

    def __init__(self, ctx=None):
        self.games = {}
        self.clients = {}

    def buildProtocol(self, addr):
        c = Client(self)
        c.id = uuid4()
        self.clients[c.id] = c
        return c

    def create_game(self, client):
        client.send("Creating a new game")
        g = Game()
        self.games[g.id] = g
        return g.add(client)

    def join_game(self, gameid, client):
        g = self.games[gameid]
        client.send("Joining game %s" % g.id)        
        g.add(client)

    @staticmethod
    def argparser():
        parser = argparse.ArgumentParser(description="Battleship Server")
        parser.add_argument('-p', dest="port", default=23, type=int,
                            help="Port to run telnet server")
        return parser.parse_args()
    
    @classmethod
    def _run(cls, port=23):
        """Launches a telnet Battleship Server on specified port"""        
        reactor.listenTCP(port, cls())
        reactor.run()

    @classmethod
    def run(cls):
        args = cls.argparser()
        return cls._run(args.port)


if __name__ == "__main__":
    args = Server.argparser()
    Server.run(args.port)
