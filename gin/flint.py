#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    flint.py
    ~~~~~~~~

    For linting: Dependency Injection for python (think pex).

    :copyright: (c) 2016 by mek.
    :license: see LICENSE for more details.
"""

import os
import argparse


class Flint(object):
    """
    Note to interviewer (Richard): I had to do something similar to
    `inject_deps` in https://github.com/mekarpeles/pypc which handled
    scaffolding for python projects. We can also use something like
    `pex` (https://github.com/pantsbuild/pex)

    The following isn't optimized to consider many factors, such as
    relative imports specified within a project's __init__.py (which
    are not explicitly referenced by the file) or module-level imports
    (such as from foo import bar or import foo.bar).
    """
    def __init__(self, filepath):
        self.path = filepath.rsplit(os.sep, 1) \
                    if os.sep in filepath else os.getcwd()
        self.filename = filepath.rsplit(os.sep, 1)[-1]

    @property
    def inject_deps(self):
        """Recursively merges all a python file's *project* dependencies
        (i.e. those who share its path) into a single string. An
        optimization would be to use temporary files for both `deps`
        and the `accum`. Also, since some files may have a massive
        amount of imports, we may wish to explore writing this in an
        interative fashion to avoid max recursion depth (i.e. blowing the
        stack).
        
        Warning: Keeping track of already imported dependencies can
        get messy, especially since an import can be dependent on the
        flow of a program's execution (e.g. a dep is imported in the
        inner-scope of a function call). Ideally, we'd want to give
        precedence/priority to higher scoped imports, but (a) our
        visited `deps` list is not sufficiently architected to recall
        the depth of an import -- which we can address by changing the
        data structure to be tuples of (import_name, depth) instead of
        import_name -- and (b) there then exists a challenge of
        retroactively prioritizing and reconsiling higher scoped
        imports are discovered after more deeper scoped imports.
        Perhaps this merits a preliminary pass/sweep strategy to
        preemptively determine which imports should be choosen for a
        given file, prior to recursive expansion.
        """    
        def _process(fname, deps=None, accum="", depth=0):
            fpath = '%s%s%s' % (self.path, os.sep, fname)
            with open(fpath, 'r') as f:
                for line in f:
                    if line.lstrip().startswith('import '):
                        # In case import statement nested within scope,
                        # preserve indentation when injecting dependency
                        indent = len(line) - len(line.lstrip())
                        dep = "%s.py" % line.strip()[7:].replace('\n', '').split('.')[0]
        
                        # prevent redundant imports (both project and
                        # system level.
                        if dep not in deps:
                            if os.path.isfile(dep):  # only consider project files
                                deps.append(dep)
                                accum += "# import %s\n" % dep
                                accum += _process(dep, deps=deps, accum="", depth=indent)
                            else:
                                accum += (" " * depth) + line
                    else:
                        accum += (" " * depth) + line
            return accum
        return _process(self.filename, deps=[])

    @classmethod
    def argparser(cls):
        parser = argparse.ArgumentParser(
            description=cls.inject_deps.__doc__
        )
        parser.add_argument('-f', dest="filepath",
                            help="Input source file")
        return parser.parse_args()

    @classmethod
    def run(cls):
        args = cls.argparser()
        if args.filepath:
            return cls(args.filepath).inject_deps

        
if __name__ == "__main__":
    Flint.run()
    
