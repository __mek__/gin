#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    combobreaker.py
    ~~~~~~~~~~~~~~~

    Given a combination length and a number of values/choices for each
    combination digit, combobreaker will exploit De Bruijn graphs to
    produce a near-optimal input sequence for cracking the passcode.

    :copyright: (c) 2016 by mek.
    :license: see LICENSE for more details.
"""

import argparse

try:
    irange = xrange
except NameError:
    irange = range


class Combobreaker(object):
    """Note to intervier (Luis): This approaches but does not guarantee an
    optimal solution (it doesn't iterate over all possibly orderings
    of prefix choices)
    """
    
    @staticmethod
    def crack(combolen=4, choices=10):
        """Exploiting De Bruijn graphs to unlock safe combinations"""
        if choices > 10:
            raise ValueError("Exploit limited to 10 choices")

        prefixes = dict()
        maxdigit = choices-1  # highest avail. combo digit of our choices
        for i in irange(int(str(maxdigit) * (combolen-1)) + 1):
            prefixes['%s'.zfill(combolen) % i] = [
                str(final) for final in irange(choices)
            ]
            
        # choose initial combo of all 0's and remove from avail. combos
        sol = "0" * combolen                    
        prefixes["0" * (combolen-1)].remove("0")
        if not prefixes["0" * (combolen-1)]:
            del prefixes["0" * (combolen-1)]
    
        while prefixes:
            prefix = sol[-(combolen-1):]
            if prefix in prefixes:
                sol +=  prefixes[prefix].pop(0)
                # if it's out of finals, delete the key
                if not prefixes[prefix]:
                    del prefixes[prefix]
            else:
                # if no prefix is set or no candidate found,
                # choose any prefix and remove it as avail. combo
                p, v = prefixes.popitem()
                sol += p + v.pop(0)
                if v:
                    prefixes[p] = v
        return sol

    @classmethod
    def argparser(cls):
        parser = argparse.ArgumentParser(description=cls.crack.__doc__)
        parser.add_argument('-l', dest="combolen", default=4, type=int,
                            help="Length of combination")
        parser.add_argument('-c', dest="choices", default=10, type=int,
                            help="Choices per digit")
        return parser.parse_args()
    
    @classmethod
    def run(cls):
        args = cls.argparser()
        return cls.crack(combolen=args.combolen, choices=args.choices)


if __name__ == "__main__":
    Combobreaker.run()
