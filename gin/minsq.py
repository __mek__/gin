#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    __init__.py
    ~~~~~~~~~~~

    Google Interview Questions

    :copyright: (c) 2016 by mek.
    :license: see LICENSE for more details.
"""

import argparse
from math import sqrt, floor
from utils import shortest

try:
    irange = xrange
except NameError:
    irange = range
    

class Minsq(object):
    """Minsquares algorithm for Ted"""

    # Base cases for memoization
    SHORTEST_SQS = {
        3: [1, 1, 1],
        2: [1, 1],
        1: [1]
    }

    @classmethod
    def minsquares(cls, n, lenonly=False):
        """Minsq is a program which solves the
        minimum list of square numbers necessary which sum to a number `n` in
        O(sqrt(n) * n) time.

        usage:
            >>> Interview4.minsquares(10000000)
            [3160, 120]
        """
        def _min(n):
            if n not in cls.SHORTEST_SQS:
                solutions = []
                max_sq = int(floor(sqrt(float(n))))
                for i in reversed(irange(2, max_sq+1)):
                    sol, rem = [], n
                    # decrease max_sq to check for
                    # alternate solutions:
                    for j in reversed(irange(1, i+1)):
                        times = rem // (j**2)
                        sol += [j for k in irange(times)]
                        rem -= (j**2) * times
                        if rem in cls.SHORTEST_SQS:
                            sol += cls.SHORTEST_SQS[rem]
                            break
                        if not rem:
                            break
                    solutions.append(sol)
                best = shortest(solutions)
                cls.SHORTEST_SQS[n] = best
            else:
                best = cls.SHORTEST_SQS[n]
            return len(best) if lenonly else best
        return _min(n)

    @classmethod
    def argparser(cls):
        parser = argparse.ArgumentParser(
            description=cls.minsquares.__doc__
        )
        parser.add_argument('-n', dest="n", type=int,
                            help="A number whose sum of squares to minimize")
        parser.add_argument('--len', dest="lenonly", action='store_true',
                            default=False, help="Return the number of terms " \
                            "(length) of the solution")
        return parser.parse_args()


    @classmethod
    def run(cls):
        args = cls.argparser()
        return cls.minsquares(args.n, lenonly=args.lenonly)


if __name__ == "__main__":
    Minsq.run()
