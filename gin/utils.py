#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    utils.py
    ~~~~~~~~

    Google Interview Question general utilities

    :copyright: (c) 2016 by mek.
    :license: see LICENSE for more details.
"""

def shortest(lst):
    """Finds the shortest sub-list of a list
    
    usage:
        >>> shortest([[1,2,3], [1,2], [1]])
        [1]
    """
    if not isinstance(lst, list):
        raise TypeError(
            "Cannot find shortest sub-list in type: %s" % type(lst)
        )
    if not lst:
        raise ValueError("Cannot find shortest sub-list of an empty list")
    
    shortest = lst.pop(0)
    llen = len(shortest)
    for candidate in lst:
        llen2 = len(candidate)
        if llen2 < llen:
            shortest, llen = candidate, llen2
    return shortest
